import { Album } from 'src/albums/album.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from 'typeorm';

@Entity()
export class Song extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: String;
  @Column()
  duration: number;

  @OneToMany(() => Album, (album) => album.song, { onDelete: 'CASCADE' })
  albums: Album[];
}
