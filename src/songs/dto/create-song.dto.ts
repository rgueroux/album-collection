import { IsNotEmpty } from 'class-validator';

export class createSongDto {
  @IsNotEmpty()
  title: string;
  @IsNotEmpty()
  duration: number;
}
