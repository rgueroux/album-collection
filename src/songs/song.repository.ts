import { EntityRepository } from 'typeorm';
import { Repository } from 'typeorm/repository/Repository';
import { createSongDto } from './dto/create-song.dto';
import { Song } from './song.entity';

@EntityRepository(Song)
export class SongRepository extends Repository<Song> {
  async createSong(createSongDto: createSongDto): Promise<Song> {
    const { title, duration } = createSongDto;

    const song = new Song();
    song.title = title;
    song.duration = duration;
    await song.save();

    return song;
  }

  private songs: Song[] = [];
  async getAllSongs(): Promise<Song[]> {
    this.songs = await Song.find();
    return this.songs;
  }

  async deleteSong(id: string) {
    await Song.delete(id);
  }
}
