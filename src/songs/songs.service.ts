import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SongRepository } from './song.repository';
import { Song } from './song.entity';
import { createSongDto } from './dto/create-song.dto';

@Injectable()
export class SongsService {
  constructor(
    @InjectRepository(SongRepository)
    private songRepository: SongRepository,
  ) {}

  async createSong(createSongDto: createSongDto): Promise<Song> {
    const song: Song = await this.songRepository.createSong(createSongDto);
    return song;
  }

  async getAllSongs(): Promise<Song[]> {
    return await this.songRepository.createQueryBuilder('songs').getMany();
  }

  async getSongById(id: string): Promise<Song> {
    const song = await this.songRepository
      .createQueryBuilder('songs')
      .where('songs.id = :songId', { songId: id })
      .getOne();
    if (!song) {
      throw new NotFoundException('No song found');
    }
    return song;
  }

  async deleteSongById(id: string): Promise<Song[]> {
    if (this.getSongById(id)) {
      await this.songRepository.deleteSong(id);
    }
    const songs: Song[] = await this.getAllSongs();
    return songs;
  }

  async updateSongById(id: string, title: string, duration: number): Promise<Song> {
    const found: Song = await this.getSongById(id);
    console.log(found);
    found.duration = duration;
    found.title = title;
    await found.save();
    return found;
  }
}
