import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Song } from './song.entity';
import { SongsService } from './songs.service';
import { createSongDto } from './dto/create-song.dto';

@Controller('songs')
export class SongsController {
  constructor(private songsService: SongsService) {}

  @Get()
  async getAllSongs(): Promise<Song[]> {
    return this.songsService.getAllSongs();
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createSong(@Body() createSongDto: createSongDto): Promise<Song> {
    return this.songsService.createSong(createSongDto);
  }

  @Get('/:id')
  async getSongById(@Param('id') id: string): Promise<Song> {
    return this.songsService.getSongById(id);
  }

  @Delete(':id')
  async deleteSongById(@Param('id') id: string): Promise<Song[]> {
    return this.songsService.deleteSongById(id);
  }

  @Patch()
  async updateSongTitleById(
    @Body('id') id: string,
    @Body('title') title: string,
    @Body('duration') duration: number,
  ): Promise<Song> {
    return this.songsService.updateSongById(id, title, duration);
  }
}
