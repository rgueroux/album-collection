import { Max, Min } from 'class-validator';
import { Artist } from 'src/artists/artists.entity';
import { Song } from 'src/songs/song.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Album extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: String;
  @Column()
  @Min(8)
  @Max(8)
  year: number;
  @Column()
  cover: String;

  @ManyToOne(() => Artist, (artist) => artist.albums, { onDelete: 'CASCADE' })
  artist: Artist;
  @ManyToOne(() => Song, (song) => song.albums, { onDelete: 'CASCADE' })
  song: Song;
}
