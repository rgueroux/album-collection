import { IsNotEmpty } from 'class-validator';

export class createAlbumDto {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  year: number;

  @IsNotEmpty()
  cover: String;

  @IsNotEmpty()
  artist: number;

  @IsNotEmpty()
  song: number;
}

export class updateAlbumDto {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  year: number;

  @IsNotEmpty()
  cover: String;

  @IsNotEmpty()
  artist: number;

  @IsNotEmpty()
  song: number;
}
