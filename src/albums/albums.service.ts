import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AlbumRepository } from './album.repository';
import { Album } from './album.entity';
import { createAlbumDto } from './dto/create-album.dto';
import { updateAlbumDto } from './dto/create-album.dto';
import { Artist } from 'src/artists/artists.entity';
import { ArtistRepository } from 'src/artists/artist.repository';
import { SongRepository } from 'src/songs/song.repository';
import { Song } from 'src/songs/song.entity';

@Injectable()
export class AlbumsService {
  constructor(
    @InjectRepository(AlbumRepository)
    private albumRepository: AlbumRepository,
    @InjectRepository(ArtistRepository)
    private artistRepository: ArtistRepository,
    @InjectRepository(SongRepository)
    private songRepository: SongRepository,
  ) {}

  async createAlbum(createAlbumDto: createAlbumDto): Promise<Album> {
    const artist: Artist = await this.artistRepository.findOne(
      createAlbumDto.artist,
    );
    const song: Song = await this.songRepository.findOne(createAlbumDto.song);

    if (!artist) {
      throw new NotFoundException('No artist found');
    }
    if (!song) {
      throw new NotFoundException('No song found');
    }
    return await this.albumRepository.createAlbum(createAlbumDto, artist, song);
  }

  async getAllAlbums(): Promise<Album[]> {
    return await this.albumRepository
      .createQueryBuilder('albums')
      .leftJoinAndSelect('albums.artist', 'artist')
      .leftJoinAndSelect('albums.song', 'song')
      .getMany();
  }

  async getAlbumById(id: string): Promise<Album> {
    const found = await this.albumRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Album with ID "${id}" not found`);
    }

    return found;
  }

  async deleteAlbumById(id: string): Promise<Album[]> {
    if (this.getAlbumById(id)) {
      await this.albumRepository.deleteAlbum(id);
    }
    const albums: Album[] = await this.getAllAlbums();
    return albums;
  }

  async updateAlbum(
    updateAlbumDto: updateAlbumDto,
    id: string,
  ): Promise<Album> {
    let album: Album = await this.albumRepository.findOne(id);
    return await this.albumRepository.updateAlbum(updateAlbumDto, album);
  }
}
