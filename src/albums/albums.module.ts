import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArtistRepository } from 'src/artists/artist.repository';
import { ArtistsModule } from 'src/artists/artists.module';
import { SongRepository } from 'src/songs/song.repository';
import { AlbumRepository } from './album.repository';
import { AlbumsController } from './albums.controller';
import { AlbumsService } from './albums.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([AlbumRepository]),
    TypeOrmModule.forFeature([ArtistRepository]),
    TypeOrmModule.forFeature([SongRepository]),
  ],

  controllers: [AlbumsController],
  providers: [AlbumsService],
})
export class AlbumsModule {}
