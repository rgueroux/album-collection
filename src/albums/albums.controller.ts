import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Album } from './album.entity';
import { AlbumsService } from './albums.service';
import { createAlbumDto } from './dto/create-album.dto';
import { updateAlbumDto } from './dto/create-album.dto';

@Controller('albums')
export class AlbumsController {
  constructor(private albumsService: AlbumsService) {}

  @Get()
  async getAllAlbums(): Promise<Album[]> {
    return this.albumsService.getAllAlbums();
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createAlbum(@Body() createAlbumDto: createAlbumDto): Promise<Album> {
    return this.albumsService.createAlbum(createAlbumDto);
  }

  @Get('/:id')
  async getAlbumById(@Param('id') id: string): Promise<Album> {
    return this.albumsService.getAlbumById(id);
  }

  @Delete(':id')
  async deleteAlbumById(@Param('id') id: string): Promise<Album[]> {
    return this.albumsService.deleteAlbumById(id);
  }

  @Patch()
  updateAlbum(
    @Body() updateAlbumDto: updateAlbumDto,
    @Param('id') id: string,
  ): Promise<Album> {
    return this.albumsService.updateAlbum(updateAlbumDto, id);
  }
}
