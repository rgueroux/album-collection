import { Artist } from 'src/artists/artists.entity';
import { Song } from 'src/songs/song.entity';
import { EntityRepository } from 'typeorm';
import { Repository } from 'typeorm/repository/Repository';
import { Album } from './album.entity';
import { createAlbumDto } from './dto/create-album.dto';
import { updateAlbumDto } from './dto/create-album.dto';

@EntityRepository(Album)
export class AlbumRepository extends Repository<Album> {
  async createAlbum(
    createAlbumDto: createAlbumDto,
    artist: Artist,
    song: Song,
  ): Promise<Album> {
    const { title, year, cover } = createAlbumDto;

    const album = new Album();
    album.title = title;
    album.year = year;
    album.cover = cover;
    album.artist = artist;
    album.song = song;

    await album.save();

    return album;
  }

  async updateAlbum(
    updateAlbumDto: updateAlbumDto,
    album: Album,
  ): Promise<Album> {
    const { title, year, cover } = updateAlbumDto;
    album.title = title;
    album.year = year;
    album.cover = cover;
    return await album.save();
  }

  async deleteAlbum(id: string) {
    await Album.delete(id);
  }
}
