import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ArtistRepository } from './artist.repository';
import { Artist } from './artists.entity';
import { createArtistDto } from './dto/create-artist.dto';

@Injectable()
export class ArtistsService {
  constructor(
    @InjectRepository(ArtistRepository)
    private artistRepository: ArtistRepository,
  ) {}

  async createArtist(createArtistDto: createArtistDto): Promise<Artist> {
    const artist: Artist = await this.artistRepository.createArtist(
      createArtistDto,
    );
    return artist;
  }

  async getAllArtists(): Promise<Artist[]> {
    return await this.artistRepository
      .createQueryBuilder('artists')
      .leftJoinAndSelect('artists.albums', 'albums')
      .getMany();
  }

  async getArtistById(id: string): Promise<Artist> {
    const artist = await this.artistRepository
      .createQueryBuilder('artists')
      .where('artists.id = :artistId', { artistId: id })
      .getOne();
    if (!artist) {
      throw new NotFoundException('No artist found');
    }
    return artist;
  }

  async deleteArtistById(id: string): Promise<Artist[]> {
    if (this.getArtistById(id)) {
      await this.artistRepository.deleteArtist(id);
    }
    const artists: Artist[] = await this.getAllArtists();
    return artists;
  }

  async updateArtistById(
    id: string,
    name: string,
    isBand: boolean,
  ): Promise<Artist> {
    const found: Artist = await this.getArtistById(id);
    found.name = name;
    found.isBand = isBand;
    return await found.save();
  }
}
