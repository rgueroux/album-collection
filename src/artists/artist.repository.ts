import { EntityRepository } from 'typeorm';
import { Repository } from 'typeorm/repository/Repository';
import { Artist } from './artists.entity';
import { createArtistDto } from './dto/create-artist.dto';

@EntityRepository(Artist)
export class ArtistRepository extends Repository<Artist> {
  async createArtist(createArtistDto: createArtistDto): Promise<Artist> {
    const { name, isBand } = createArtistDto;
    const artist = new Artist();
    artist.name = name;
    artist.isBand = isBand;
    await artist.save();

    return artist;
  }

  async deleteArtist(id: string) {
    await Artist.delete(id);
  }
}
