import { Album } from 'src/albums/album.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from 'typeorm';

@Entity()
export class Artist extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  isBand: Boolean;

  @OneToMany(() => Album, (album) => album.artist, { onDelete: 'CASCADE' })
  albums: Album[];
}
