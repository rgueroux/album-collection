import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Artist } from './artists.entity';
import { ArtistsService } from './artists.service';
import { createArtistDto } from './dto/create-artist.dto';

@Controller('artists')
export class ArtistsController {
  constructor(private artistsService: ArtistsService) {}

  @Get()
  async getAllArtists(): Promise<Artist[]> {
    return this.artistsService.getAllArtists();
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createArtist(
    @Body() createArtistDto: createArtistDto,
  ): Promise<Artist> {
    return this.artistsService.createArtist(createArtistDto);
  }

  @Get('/:id')
  async getArtistById(@Param('id') id: string): Promise<Artist> {
    return this.artistsService.getArtistById(id);
  }

  @Delete(':id')
  async deleteArtistById(@Param('id') id: string): Promise<Artist[]> {
    return this.artistsService.deleteArtistById(id);
  }

  @Patch()
  async updateArtistById(
    @Body('id') id: string,
    @Body('name') name: string,
    @Body('isBand') isBand: boolean,
  ): Promise<Artist> {
    return this.artistsService.updateArtistById(id, name, isBand);
  }
}
